# Sublime Text 3

![travis-ci](https://travis-ci.org/KeyboardInterrupt/ansible-role-sublime-text-3.svg?branch=master)

This role installs Sublime Text 3 on Linux

## Requirements

- Ansible >= 2.1 

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

### `sublime_build`

This variable determines which version/build of Sublime Text 3 should be installed.

The default value is `"3143"`

__This variable is ignored if the [webupd8 PPA](http://www.webupd8.org/2013/07/sublime-text-3-ubuntu-ppa-now-available.html) is used.__

### `use_webupd8team_ppa`

This variable determines if the [webupd8 PPA](http://www.webupd8.org/2013/07/sublime-text-3-ubuntu-ppa-now-available.html) should be used. (Ubuntu only!)

The default value is `False`

__This variable is ignored if the target System is not Ubuntu!__

## Example Playbook


This example Playbook installs Sublime Text 3 on your local machine using the [webupd8 PPA](http://www.webupd8.org/2013/07/sublime-text-3-ubuntu-ppa-now-available.html)

```yaml
---
- hosts: localhost
  roles:
    - sublime-text-3
  vars:
    use_webupd8team_ppa: True
```

## License

[GPLv3](http://www.gnu.de/documents/gpl-3.0.de.html)

## Author Information


Author: KeyboardInterrupt
Website/Blog: [KeyboardInterrupt.com](http://KeyboardInterrupt.com)
